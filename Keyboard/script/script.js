// =================THEORY=======================
/*
1. Як можна визначити, яку саме клавішу клавіатури натиснув користувач?

window.addEventListener("keydown", function(el){...})
Таким чином ми в глобальному вікні ловимо подію натискання на клавішу

2. Яка різниця між event.code() та event.key()?

event.code() є методом, який виводить нам фізичний клавішу клавіатури (Key R) не дивлячись яка у нас вибрана мова, в той час як event.key() виводить нам лише значення того символу який відповідає нашій вибраній мові.

3. Які три події клавіатури існує та яка між ними відмінність?

keydown - коли натиснули на клавішу
keyup - коли відтиснули клавішу
keypress - показує що символ був введений. Не генерує клавіши управління (Ctrl, Alt, F1 ...)
*/

// =================PRACTICE==================

const buttons = document.querySelectorAll('.btn');

window.addEventListener("keydown", function(element){
	buttons.forEach((key) => {
		if(element.key.toUpperCase() != key.textContent.toUpperCase()){
			console.log(key.style.backgroundColor = "black");
		}else{
			console.log(key.style.backgroundColor = "blue");
		}
	})
});

